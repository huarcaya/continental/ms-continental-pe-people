﻿using Continental.PE.People.Infrastructure.Common;

using Microsoft.Extensions.Logging;

namespace Continental.PE.People.Infrastructure.Logger;

public class LoggerAdapter<T> : IAppLogger<T>
{
    private readonly ILogger _logger;

    public LoggerAdapter(ILoggerFactory loggerFactory)
    {
        _logger = loggerFactory.CreateLogger<T>();
    }

    public void LogError(string message, params object[] args)
    {
        _logger.LogError(message, args);
    }

    public void LogInformation(string message, params object[] args)
    {
        _logger.LogInformation(message, args);
    }

    public void LogWarning(string message, params object[] args)
    {
        _logger.LogWarning(message, args);
    }
}
