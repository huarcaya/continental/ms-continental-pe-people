using FluentValidation.Results;

namespace Continental.PE.People.Infrastructure.Common;

public class Response<T>
{
    public T Data { get; set; }
    public string Message { get; set; }
    public IEnumerable<ValidationFailure> Error { get; set; }
}