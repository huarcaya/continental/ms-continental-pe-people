using System.Data;

namespace Continental.PE.People.Infrastructure.Common;

public interface IConnectionFactory
{
    IDbConnection GetDbConnection { get; }
}
