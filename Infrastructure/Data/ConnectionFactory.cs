﻿using System.Data;

using Continental.PE.People.Infrastructure.Common;

using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Continental.PE.People.Infrastructure.Data;

public class ConnectionFactory : IConnectionFactory
{
    private readonly IConfiguration _configuration;

    public ConnectionFactory(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public IDbConnection GetDbConnection
    {
        get
        {
            var sqlConnection = new SqlConnection();

            if (sqlConnection == null)
            {
                return null;
            }

            sqlConnection.ConnectionString = _configuration.GetConnectionString("ucDbConnection");
            sqlConnection.Open();

            return sqlConnection;
        }
    }
}
