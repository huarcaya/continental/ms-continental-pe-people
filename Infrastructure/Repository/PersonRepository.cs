﻿using AutoMapper;
using Dapper;

using Continental.PE.People.Domain.Entity;
using Continental.PE.People.Infrastructure.Interface;
using Continental.PE.People.Infrastructure.Common;
using Continental.PE.People.Infrastructure.Entity;

namespace Continental.PE.People.Infrastructure.Repository;

public class PersonRepository : IPersonRepository
{
    private readonly IConnectionFactory _connectionFactory;
    private readonly IMapper _mapper;

    public PersonRepository(IConnectionFactory connectionFactory, IMapper mapper)
    {
        _connectionFactory = connectionFactory;
        _mapper = mapper;
    }

    #region Async Methods

    async Task<Person> IPersonRepository.FindOneByDniAsync(string dni)
    {
        using var connection = _connectionFactory.GetDbConnection;

        var query = @"SELECT IDPersona, IDPersonaN, Nombres, Appat, ApMat, IDUsuario, DNI
                    FROM dbo.tblPersona
                    WHERE DNI=@Dni";
        var requestDb = await connection.QueryFirstOrDefaultAsync<PersonEntity>(query, new { Dni = dni });

        return _mapper.Map<Person>(requestDb);
    }

    async Task<Person> IPersonRepository.FindOneByPidmAsync(int pidm)
    {
        using var connection = _connectionFactory.GetDbConnection;

        var query = @"SELECT IDPersona, IDPersonaN, Nombres, Appat, ApMat, IDUsuario, DNI
                    FROM dbo.tblPersona
                    WHERE IDPersonaN=@Pidm";
        var requestDb = await connection.QueryFirstOrDefaultAsync<PersonEntity>(query, new { Pidm = pidm });

        return _mapper.Map<Person>(requestDb);
    }

    async Task<Person> IPersonRepository.FindOneByUsernameAsync(string username)
    {
        using var connection = _connectionFactory.GetDbConnection;

        var query = @"SELECT IDPersona, IDPersonaN, Nombres, Appat, ApMat, IDUsuario, DNI
                    FROM dbo.tblPersona
                    WHERE IDUsuario=@Username";
        var requestDb = await connection.QueryFirstOrDefaultAsync<PersonEntity>(query, new { Username = username });

        return _mapper.Map<Person>(requestDb);
    }

    #endregion Async Methods

    #region Sync Methods

    public Person FindOneByDni(string dni)
    {
        using var connection = _connectionFactory.GetDbConnection;

        var query = @"SELECT IDPersona, IDPersonaN, Nombres, Appat, ApMat, IDUsuario, DNI
                    FROM dbo.tblPersona
                    WHERE DNI=@Dni";
        var requestDb = connection.QueryFirstOrDefault<PersonEntity>(query, new { Dni = dni });

        return _mapper.Map<Person>(requestDb);
    }

    public Person FindOneByPidm(int pidm)
    {
        using var connection = _connectionFactory.GetDbConnection;

        var query = @"SELECT IDPersona, IDPersonaN, Nombres, Appat, ApMat, IDUsuario, DNI
                    FROM dbo.tblPersona
                    WHERE IDPersonaN=@Pidm";
        var requestDb = connection.QueryFirstOrDefault<PersonEntity>(query, new { Pidm = pidm });

        return _mapper.Map<Person>(requestDb);
    }

    public Person FindOneByUsername(string username)
    {
        using var connection = _connectionFactory.GetDbConnection;

        var query = @"SELECT IDPersona, IDPersonaN, Nombres, Appat, ApMat, IDUsuario, DNI
                    FROM dbo.tblPersona
                    WHERE IDUsuario=@Username";
        var requestDb = connection.QueryFirstOrDefault<PersonEntity>(query, new { Username = username });

        return _mapper.Map<Person>(requestDb);
    }

    #endregion Sync Methods
}
