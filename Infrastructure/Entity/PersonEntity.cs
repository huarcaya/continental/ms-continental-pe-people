﻿namespace Continental.PE.People.Infrastructure.Entity;

public class PersonEntity
{
    public string IDPersona { get; set; }
    public string Appat { get; set; }
    public string ApMat { get; set; }
    public string Nombres { get; set; }
    public string FotoRuta { get; set; }
    public string Sexo { get; set; }
    public DateTime FecNac { get; set; }
    public string DNI { get; set; }
    public string IDUsuario { get; set; }
    public byte[] Foto { get; set; }
    public int IDPersonaN { get; set; }
}
