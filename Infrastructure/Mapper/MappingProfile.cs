﻿using AutoMapper;

using Continental.PE.People.Application.DTO;
using Continental.PE.People.Domain.Entity;
using Continental.PE.People.Infrastructure.Entity;

namespace Continental.PE.People.Infrastructure.Mapper;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<Person, PersonDto>()
            .ForMember(destination => destination.LastName, source => source.MapFrom(src => $"{src.FirstLastName} {src.SecondLastName}"))
            .ReverseMap();

        CreateMap<Person, PersonEntity>()
            .ForMember(destination => destination.Nombres, source => source.MapFrom(src => src.FirstName))
            .ForMember(destination => destination.Appat, source => source.MapFrom(src => src.FirstLastName))
            .ForMember(destination => destination.ApMat, source => source.MapFrom(src => src.SecondLastName))
            .ForMember(destination => destination.DNI, source => source.MapFrom(src => src.Dni))
            .ForMember(destination => destination.IDPersona, source => source.MapFrom(src => src.PersonId))
            .ForMember(destination => destination.IDPersonaN, source => source.MapFrom(src => src.Pidm))
            .ForMember(destination => destination.IDUsuario, source => source.MapFrom(src => src.UserId))
            .ReverseMap();
    }
}
