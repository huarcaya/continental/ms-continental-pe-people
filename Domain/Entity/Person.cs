﻿namespace Continental.PE.People.Domain.Entity;

public class Person
{
    public string PersonId { get; set; }
    public string Pidm { get; set; }
    public string FirstName { get; set; }
    public string FirstLastName { get; set; }
    public string SecondLastName { get; set; }
    public string UserId { get; set; }
    public string Dni { get; set; }
}
