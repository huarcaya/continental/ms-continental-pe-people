﻿using Continental.PE.People.Domain.Entity;

namespace Continental.PE.People.Domain.Interface;
public interface IPersonDomain
{
    #region Sync Methods
    Person FindOneByDni(string dni);
    Person FindOneByPidm(int pidm);
    Person FindOneByUsername(string username);
    #endregion Sync Methods

    #region Async Methods
    Task<Person> FindOneByDniAsync(string dni);
    Task<Person> FindOneByPidmAsync(int pidm);
    Task<Person> FindOneByUsernameAsync(string username);
    #endregion Async Methods
}
