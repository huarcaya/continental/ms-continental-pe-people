﻿using Continental.PE.People.Domain.Entity;
using Continental.PE.People.Domain.Interface;
using Continental.PE.People.Infrastructure.Interface;

namespace Continental.PE.People.Domain.Core;

public class PersonDomain : IPersonDomain
{
    private readonly IPersonRepository _personRepository;

    public PersonDomain(IPersonRepository personRepository)
    {
        _personRepository = personRepository;
    }

    #region Async Methods

    public async Task<Person> FindOneByDniAsync(string dni)
    {
        return await _personRepository.FindOneByDniAsync(dni);
    }

    public async Task<Person> FindOneByPidmAsync(int pidm)
    {
        return await _personRepository.FindOneByPidmAsync(pidm);
    }

    public async Task<Person> FindOneByUsernameAsync(string username)
    {
        return await _personRepository.FindOneByUsernameAsync(username);
    }

    #endregion Async Methods

    #region Sync Methods

    public Person FindOneByDni(string dni)
    {
        return _personRepository.FindOneByDni(dni);
    }

    public Person FindOneByPidm(int pidm)
    {
        return _personRepository.FindOneByPidm(pidm);
    }

    public Person FindOneByUsername(string username)
    {
        return _personRepository.FindOneByUsername(username);
    }

    #endregion Sync Methods
}
