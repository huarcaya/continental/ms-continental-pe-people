using Continental.PE.People.Application.Interface;

using Microsoft.AspNetCore.Mvc;

namespace Continental.PE.People.Presentation.WebAPI.Controllers;

[ApiController]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
public class PeopleController : ControllerBase
{

    private readonly IPersonApplication _personApplication;
    private readonly ILogger<PeopleController> _logger;

    public PeopleController(IPersonApplication personApplication, ILogger<PeopleController> logger)
    {
        _personApplication = personApplication;
        _logger = logger;
    }

    [HttpGet("{pidm}")]
    public async Task<IActionResult> GetByPidm(int pidm)
    {
        var response = await _personApplication.GetByPidmAsync(pidm);

        if (response.Data == null)
        {
            return NotFound(response);
        }

        return Ok(response);
    }

    [HttpGet("dni/{dni}")]
    public async Task<IActionResult> GetByDni(string dni)
    {
        var response = await _personApplication.GetByDniAsync(dni);

        if (response.Data == null)
        {
            return NotFound(response);
        }

        return Ok(response);
    }

    [HttpGet("username/{username}")]
    public async Task<IActionResult> GetByUsername(string username)
    {
        var response = await _personApplication.GetByUsernameAsync(username);

        if (response.Data == null)
        {
            return NotFound(response);
        }

        return Ok(response);
    }
}
