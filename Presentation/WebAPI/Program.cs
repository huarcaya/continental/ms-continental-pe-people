using Microsoft.AspNetCore.Mvc.ApiExplorer;

using Continental.PE.People.Infrastructure.Mapper;
using Continental.PE.People.Presentation.WebAPI.Extensions.Injection;
using Continental.PE.People.Presentation.WebAPI.Extensions.Swagger;
using Continental.PE.People.Presentation.WebAPI.Extensions.Versioning;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddVersioning();
builder.Services.AddSwagger();

builder.Services.AddRouting(options => options.LowercaseUrls = true);
builder.Services.AddAutoMapper(options => options.AddProfile(new MappingProfile()));
builder.Services.AddInjection();

var app = builder.Build();
var provider = app.Services.GetRequiredService<IApiVersionDescriptionProvider>();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        foreach (var description in provider.ApiVersionDescriptions)
        {
            options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName);
        }
    });
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
