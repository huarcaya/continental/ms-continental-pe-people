using Continental.PE.People.Application.Interface;
using Continental.PE.People.Application.Main;
using Continental.PE.People.Domain.Core;
using Continental.PE.People.Domain.Interface;
using Continental.PE.People.Infrastructure.Common;
using Continental.PE.People.Infrastructure.Data;
using Continental.PE.People.Infrastructure.Interface;
using Continental.PE.People.Infrastructure.Repository;

namespace Continental.PE.People.Presentation.WebAPI.Extensions.Injection;

public static class InjectionExtensions
{
    public static IServiceCollection AddInjection(this IServiceCollection services)
    {
        services.AddSingleton<IConnectionFactory, ConnectionFactory>();
        services.AddScoped<IPersonApplication, PersonApplication>();
        services.AddScoped<IPersonDomain, PersonDomain>();
        services.AddScoped<IPersonRepository, PersonRepository>();

        return services;
    }
}