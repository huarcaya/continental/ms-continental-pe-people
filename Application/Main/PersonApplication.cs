﻿using AutoMapper;

using Continental.PE.People.Application.DTO;
using Continental.PE.People.Application.Interface;
using Continental.PE.People.Domain.Interface;
using Continental.PE.People.Infrastructure.Common;

namespace Continental.PE.People.Application.Main;

public class PersonApplication : IPersonApplication
{
    private readonly IPersonDomain _personDomain;
    private readonly IMapper _mapper;

    public PersonApplication(IPersonDomain personDomain, IMapper mapper)
    {
        _personDomain = personDomain;
        _mapper = mapper;
    }

    #region Sync Methods

    public Response<PersonDto> GetByDni(string dni)
    {
        var response = new Response<PersonDto>();

        try
        {
            var person = _personDomain.FindOneByDni(dni);
            response.Data = _mapper.Map<PersonDto>(person);
        }
        catch (Exception e)
        {

            response.Message = e.Message;
        }

        return response;
    }

    public Response<PersonDto> GetByPidm(int pidm)
    {
        var response = new Response<PersonDto>();

        try
        {
            var person = _personDomain.FindOneByPidm(pidm);
            response.Data = _mapper.Map<PersonDto>(person);

            if (response.Data != null)
            {
                response.Message = "Consulta exitosa";
            }
        }
        catch (Exception e)
        {

            response.Message = e.Message;
        }

        return response;
    }



    public Response<PersonDto> GetByUsername(string username)
    {
        var response = new Response<PersonDto>();

        try
        {
            var person = _personDomain.FindOneByUsername(username);
            response.Data = _mapper.Map<PersonDto>(person);
        }
        catch (Exception e)
        {

            response.Message = e.Message;
        }

        return response;
    }

    #endregion Sync Methods

    #region Async Methods

    public async Task<Response<PersonDto>> GetByDniAsync(string dni)
    {
        var response = new Response<PersonDto>();

        try
        {
            var person = await _personDomain.FindOneByDniAsync(dni);
            response.Data = _mapper.Map<PersonDto>(person);
        }
        catch (Exception e)
        {

            response.Message = e.Message;
        }

        return response;
    }

    public async Task<Response<PersonDto>> GetByPidmAsync(int pidm)
    {
        var response = new Response<PersonDto>();

        try
        {
            var person = await _personDomain.FindOneByPidmAsync(pidm);
            response.Data = _mapper.Map<PersonDto>(person);

            if (response.Data != null)
            {
                response.Message = "Consulta exitosa";
            }
        }
        catch (Exception e)
        {

            response.Message = e.Message;
        }

        return response;
    }

    public async Task<Response<PersonDto>> GetByUsernameAsync(string username)
    {
        var response = new Response<PersonDto>();

        try
        {
            var person = await _personDomain.FindOneByUsernameAsync(username);
            response.Data = _mapper.Map<PersonDto>(person);
        }
        catch (Exception e)
        {

            response.Message = e.Message;
        }

        return response;
    }

    #endregion Async Methods
}
