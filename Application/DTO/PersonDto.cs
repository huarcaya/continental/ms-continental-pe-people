﻿namespace Continental.PE.People.Application.DTO;

public record PersonDto
{
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string Dni { get; set; } = string.Empty;
    public int Pidm { get; set; }
    public string PersonId { get; set; } = string.Empty;
}
