﻿using Continental.PE.People.Application.DTO;
using Continental.PE.People.Infrastructure.Common;

namespace Continental.PE.People.Application.Interface;

public interface IPersonApplication
{
    #region Async Methods
    Task<Response<PersonDto>> GetByDniAsync(string dni);
    Task<Response<PersonDto>> GetByPidmAsync(int pidm);
    Task<Response<PersonDto>> GetByUsernameAsync(string username);
    #endregion Async Methods

    #region Sync Methods
    Response<PersonDto> GetByDni(string dni);
    Response<PersonDto> GetByPidm(int pidm);
    Response<PersonDto> GetByUsername(string username);
    #endregion Sync Methods
}
